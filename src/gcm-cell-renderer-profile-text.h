/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2010-2019 Richard Hughes <richard@hughsie.com>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef GCM_CELL_RENDERER_PROFILE_TEXT_H
#define GCM_CELL_RENDERER_PROFILE_TEXT_H

G_BEGIN_DECLS

#include <glib-object.h>
#include <gtk/gtk.h>
#include <colord.h>

#define GCM_TYPE_CELL_RENDERER_PROFILE_TEXT		(gcm_cell_renderer_profile_text_get_type())
G_DECLARE_FINAL_TYPE (GcmCellRendererProfileText, gcm_cell_renderer_profile_text, GCM, CELL_RENDERER_PROFILE_TEXT, GtkCellRendererText)

GtkCellRenderer	*gcm_cell_renderer_profile_text_new		(void);

G_END_DECLS

#endif /* GCM_CELL_RENDERER_PROFILE_TEXT_H */

