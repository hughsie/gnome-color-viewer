/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2006-2019 Richard Hughes <richard@hughsie.com>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __GCM_CIE_WIDGET_H__
#define __GCM_CIE_WIDGET_H__

G_BEGIN_DECLS

#include <gtk/gtk.h>
#include <colord.h>

#define GCM_TYPE_CIE_WIDGET		(gcm_cie_widget_get_type ())
G_DECLARE_FINAL_TYPE (GcmCieWidget, gcm_cie_widget, GCM, CIE_WIDGET, GtkDrawingArea)

GtkWidget	*gcm_cie_widget_new			(void);
void		 gcm_cie_widget_set_from_profile	(GtkWidget	*widget,
							 CdIcc		*profile);

G_END_DECLS

#endif
