/* -*- Mode: C; tab-width: 8; indent-tabs-mode: t; c-basic-offset: 8 -*-
 *
 * Copyright (C) 2009-2019 Richard Hughes <richard@hughsie.com>
 *
 * Licensed under the GNU General Public License Version 2
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __GCM_UTILS_H
#define __GCM_UTILS_H

#include <colord.h>
#include <gtk/gtk.h>

const gchar	*cd_colorspace_to_localised_string	(CdColorspace		 colorspace);
gchar		*gcm_utils_linkify			(const gchar		*text);
gboolean	 gcm_utils_image_convert		(GtkImage		*image,
							 CdIcc			*input,
							 CdIcc			*abstract,
							 CdIcc			*output,
							 GError			**error);

#endif /* __GCM_UTILS_H */

